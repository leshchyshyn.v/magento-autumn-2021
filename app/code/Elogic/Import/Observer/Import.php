<?php

namespace Elogic\Import\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Import implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        echo "\n";
        echo "import observer";

        $data = $observer->getImportData();

        var_dump($data->getData());


        echo "\n";
    }
}
