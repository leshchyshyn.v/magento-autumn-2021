<?php

namespace Elogic\Import\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Product implements ObserverInterface
{
    public function execute(Observer $observer)
    {
//        die("2");
       $product = $observer->getProduct();
       $product->setName($product->getName()." observed");
    }
}
