<?php

namespace Elogic\Import\Plugins;

class ImporterSecond
{
    public function beforeExecute(\Elogic\Import\Service\Importer $importer)
    {
        echo "\n";
        echo "beforeExecute Second";
        echo "\n";
    }

    public function afterExecute(\Elogic\Import\Service\Importer $importer, $result)
    {
        echo "\n";
        echo "afterExecute Second";
        echo "\n";
        return $result;
    }
}
