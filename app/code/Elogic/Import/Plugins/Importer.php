<?php

namespace Elogic\Import\Plugins;

class Importer
{
    public function beforeExecute(\Elogic\Import\Service\Importer $importer)
    {
        echo "\n";
        echo "beforeExecute First";
        echo "\n";
    }

    public function afterExecute(\Elogic\Import\Service\Importer $importer, $result)
    {
        echo "\n";
        echo "afterExecute First";
        echo "\n";
        return $result;
    }
}
