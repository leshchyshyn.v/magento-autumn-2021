<?php

declare(strict_types=1);

namespace Elogic\Import\Service;

use Magento\Framework\DataObject;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Event\ManagerInterface as EventManager;

class Importer
{
    /**
     * @var array
     */
    private $importers;
    /**
     * @var OutputInterface
     */
    private $output;
    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * Importer constructor.
     * @param EventManager $eventManager
     * @param array $importers
     */
    public function __construct(
//        OutputInterface $output,
        EventManager $eventManager,
        array $importers = []
    )
    {
        $this->importers = $importers;
//        $this->output = $output;
        $this->eventManager = $eventManager;
    }

    public function execute()
    {

        $data = new DataObject();
        $data->setData("foo","bar");

        $this->eventManager->dispatch("importer_import_start", ["import_data" => $data]);

//        var_dump(self::class);
//        exit;
//        foreach ($this->importers as $importer) {
//            $result = $importer->execute();
//
//            if ($result) {
//                $this->output->writeln('<info>Imported!</info>');
//            } else {
//                $this->output->writeln('<error>Failed</error>');
//            }
//        }
        $this->eventManager->dispatch("importer_import_end");
    }
}