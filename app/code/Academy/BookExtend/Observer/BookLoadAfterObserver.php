<?php

namespace Academy\BookExtend\Observer;

class BookLoadAfterObserver implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $book = $observer->getBook();

        if ($book->getAuthor()=="")
        {
            $book->setAuthor("unknown");
        }
    }
}