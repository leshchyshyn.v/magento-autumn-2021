<?php

namespace Academy\Book\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * CMS page CRUD interface.
 * @api
 * @since 100.0.2
 */
interface BookRepositoryInterface
{
    /**
     * Save book.
     *
     * @param \Academy\Book\Api\Data\BookInterface $book
     * @return \Academy\Book\Api\Data\BookInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Academy\Book\Api\Data\BookInterface $book);

    /**
     * Retrieve page.
     *
     * @param int $bookId
     * @return \Academy\Book\Api\Data\BookInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($bookId);

    /**
     * Retrieve books matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Academy\Book\Api\Data\BookSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete page.
     *
     * @param \Academy\Book\Api\Data\BookInterface $book
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Academy\Book\Api\Data\BookInterface $book);

    /**
     * Delete page by ID.
     *
     * @param int $bookId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($bookId);
}
