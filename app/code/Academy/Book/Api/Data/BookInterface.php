<?php

namespace Academy\Book\Api\Data;

interface BookInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const BOOK_ID           = "book_id";
    const TITLE             = "title";
    const AUTHOR            = "author";
    const YEAR_PUBLISHED    = "year_published";
    const CREATION_TIME     = "creation_time";
    const UPDATE_TIME       = "update_time";
    /**#@-*/

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string|null
     */
    public function getAuthor();

    /**
     * @return int|null
     */
    public function getYearPublished();

    /**
     * @return string|null
     */
    public function getCreationTime();

    /**
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * @param int $id
     * @return \Academy\Book\Api\Data\BookInterface
     */
    public function setId($id);

    /**
     * @param string $title
     * @return \Academy\Book\Api\Data\BookInterface
     */
    public function setTitle($title);

    /**
     *
     * @param string $author
     * @return \Academy\Book\Api\Data\BookInterface
     */
    public function setAuthor($author);

    /**
     * @param  int $year
     * @return \Academy\Book\Api\Data\BookInterface
     */
    public function setYearPublished($year);

    /**
     * @param string $time
     * @return \Academy\Book\Api\Data\BookInterface
     */
    public function setCreationTime($time);

    /**
     * @param string $time
     * @return self
     */
    public function setUpdateTime($time);
}