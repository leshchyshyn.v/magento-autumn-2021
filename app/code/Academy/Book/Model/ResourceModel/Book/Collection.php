<?php

namespace Academy\Book\Model\ResourceModel\Book;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Book
 * @package Academy\Book\Model\ResourceModel\Collection
 */
class Collection extends AbstractCollection
{
    public function _construct() //phpcs:ignore Magento2.CodeAnalysis.EmptyBlock
    {
        parent::_init(
            \Academy\Book\Model\Book::class,
            \Academy\Book\Model\ResourceModel\Book::class
        );
    }
}