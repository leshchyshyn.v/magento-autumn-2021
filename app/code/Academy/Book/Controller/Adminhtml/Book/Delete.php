<?php

namespace Academy\Book\Controller\Adminhtml\Book;

use Academy\Book\Api\BookRepositoryInterface;
use Magento\Backend\App\Action;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Academy_Book::book';

    /**
     * @var BookRepositoryInterface
     */
    private $bookRepository;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param BookRepositoryInterface $bookRepository
     */
    public function __construct(
        Action\Context $context,
        BookRepositoryInterface $bookRepository
    )
    {
        parent::__construct($context);
        $this->bookRepository = $bookRepository;
    }

    public function execute()
    {
        $redirect = $this->resultRedirectFactory->create();

        try {
            $bookId = $this->getRequest()->getParam("book_id");
            $this->bookRepository->deleteById((int)$bookId);
            $this->messageManager->addSuccessMessage("Book was deleted");
        } catch (\Exception $ex) {
            $this->messageManager->addErrorMessage("Error on deleting book");
        }

        return $redirect->setPath("*/*/");
    }
}