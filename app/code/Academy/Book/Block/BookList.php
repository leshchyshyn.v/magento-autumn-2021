<?php

declare(strict_types=1);

namespace Academy\Book\Block;

use Academy\Book\Model\Book;
use Academy\Book\Model\ResourceModel\Book\Collection as BookCollection;
use Academy\Book\Model\ResourceModel\Book\CollectionFactory as CollectionFactory;
use Academy\Book\Helper\Data as BookHelper;
use Magento\Framework\View\Element\Template;

/**
 * Class RandomProduct
 * @package Academy\Book\Block
 */
class BookList extends Template
{
    /**
     * @var Book
     */
    private $bookModel;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var BookHelper
     */
    private $bookHelper;

    /**
     * View constructor.
     * @param Book $book
     * @param CollectionFactory $collectionFactory
     * @param BookHelper $bookHelper
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Book $book,
        CollectionFactory $collectionFactory,
        BookHelper $bookHelper,
        Template\Context $context,

        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->bookModel = $book;
        $this->collectionFactory = $collectionFactory;
        $this->bookHelper = $bookHelper;
    }

    /**
     * @return BookCollection
     */
    public function getBooks() : BookCollection
    {
        /** @var BookCollection $collection */

        $collection = $this->collectionFactory->create();

        if ($this->bookHelper->isEnabled()){
            $collection->setPageSize($this->bookHelper->getBooksPerPage());
        }

//        var_dump($this->bookHelper->isEnabled());
//        var_dump($this->bookHelper->getBooksPerPage());
//        die;

//        $collection->addFieldToFilter(
//            "year_published",
//                ["eq" => 1912]
//        );

        $collection->setOrder("year_published","asc");

        return $collection;
    }

}