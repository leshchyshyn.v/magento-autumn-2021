<?php

namespace Magebase\Example\Api;

interface AuthorRepositoryInterface
{
    public function getById($id);

    public function save($author);

    public function delete($author);

    public function deleteById($id);
}
