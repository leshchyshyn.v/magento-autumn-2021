<?php

namespace Magebase\Example\Api\Data;

interface AuthorInterface
{
    const AUTHOR_ID = 'author_id';
    const FULL_NAME = 'full_name';
    const LIVING_YEARS = 'living_years';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME = 'update_time';

    public function getId();

    public function setId($id);

    public function getFullName();

    public function setFullName($name);

    public function getLivingYears();

    public function setLivingYears($dates);

    public function getCreatedAt();

    public function getUpdatedAt();
}
