<?php

namespace Magebase\Example\Setup\Patch\Data;

use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Cms\Model\PageFactory;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class CreateNewCmsPage implements DataPatchInterface
{
    /**
     * @var PageFactory
     */
    private $pageFactory;
    /**
     * @var PageRepositoryInterface
     */
    private $pageRepository;

    /**
     * CreateNewCmsPage constructor.
     * @param PageFactory $pageFactory
     * @param PageRepositoryInterface $pageRepository
     */
    public function __construct(
        PageFactory $pageFactory,
        PageRepositoryInterface $pageRepository
    )
    {
        $this->pageFactory = $pageFactory;
        $this->pageRepository = $pageRepository;
    }

    public function apply()
    {
        $newPage = $this->pageFactory->create();
        $newPage->setTitle("Elogic Data Patch Page")
            ->setContent("Page created from Data patch")
            ->setUrlKey("elogic-page");

        $this->pageRepository->save($newPage);
    }


    public function getAliases()
    {
        return [];
    }

    public static function getDependencies()
    {
        return [];
    }
}
