<?php

namespace Magebase\Example\Model;

use Magebase\Example\Api\AuthorRepositoryInterface;
use Magebase\Example\Model\AuthorFactory;
use Magebase\Example\Model\ResourceModel\Author as ResourceAuthor;

class AuthorRepository implements AuthorRepositoryInterface
{
    /**
     * @var ResourceAuthor
     */
    private $resource;
    /**
     * @var \Magebase\Example\Model\AuthorFactory
     */
    private $authorFactory;

    /**
     * AuthorRepository constructor.
     * @param ResourceAuthor $resource
     * @param \Magebase\Example\Model\AuthorFactory $authorFactory
     */
    public function __construct(
        ResourceAuthor $resource,
        AuthorFactory $authorFactory
    )
    {

        $this->resource = $resource;
        $this->authorFactory = $authorFactory;
    }

    public function getById($id)
    {
        $author = $this->authorFactory->create();
        $this->resource->load($author, $id);
        return $author;
    }

    public function save($author)
    {
        $this->resource->save($author);
        return $author;
    }

    public function delete($author)
    {
        $this->resource->delete($author);
    }

    public function deleteById($id)
    {
        $author = $this->getById($id);
        $this->delete($author);
    }
}
