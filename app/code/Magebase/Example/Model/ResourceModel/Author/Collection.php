<?php

namespace Magebase\Example\Model\ResourceModel\Author;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct() //phpcs:ignore Magento2.CodeAnalysis.EmptyBlock
    {
        $this->_init(
            \Magebase\Example\Model\Author::class,
            \Magebase\Example\Model\ResourceModel\Author::class
        );
    }
}

