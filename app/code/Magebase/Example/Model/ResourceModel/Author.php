<?php

namespace Magebase\Example\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Author extends AbstractDb
{
    public function _construct()
    {
        $this->_init("author_new","author_id");
    }
}
