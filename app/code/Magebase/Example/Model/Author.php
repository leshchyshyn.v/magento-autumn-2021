<?php

namespace Magebase\Example\Model;

use Magebase\Example\Api\Data\AuthorInterface;

class Author extends \Magento\Framework\Model\AbstractModel implements AuthorInterface
{
    protected $_eventPrefix = "author";
    //author_save_before
    //author_save_after
    //author_load_before
    //author_load_after

    protected $_eventObject = "author";


    public function _construct() //phpcs:ignore Magento2.CodeAnalysis.EmptyBlock
    {
        $this->_init(\Magebase\Example\Model\ResourceModel\Author::class);
    }

    public function getId()
    {
        return $this->getData(self::AUTHOR_ID);
    }

    public function setId($id)
    {
        return $this->setData(self::AUTHOR_ID, $id);
    }

    public function getFullName()
    {
        return $this->getData(self::FULL_NAME);
    }

    public function setFullName($name)
    {
        return $this->setData(self::FULL_NAME, $name);
    }

    public function getLivingYears()
    {
        return $this->getData(self::LIVING_YEARS);
    }

    public function setLivingYears($dates)
    {
        return $this->setData(self::LIVING_YEARS, $dates);
    }

    public function getCreatedAt()
    {
        return $this->getData(self::CREATION_TIME);
    }

    public function getUpdatedAt()
    {
        return $this->getData(self::CREATION_TIME);
    }
}
