<?php

namespace Magebase\Example\Block;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template;

class View extends Template
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * View constructor.
     * @param ProductRepositoryInterface $productRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->productRepository = $productRepository;
    }

    public function getProduct($id = 1)
    {

//        $productRepository = ObjectManager::getInstance()->get(\Magento\Catalog\Api\ProductRepositoryInterface::class);

        $product = $this->productRepository->getById($id);

//        var_dump(get_class($this->productRepository));die;
        
        $product->setFoo("bar");

        return $product;
    }
}
