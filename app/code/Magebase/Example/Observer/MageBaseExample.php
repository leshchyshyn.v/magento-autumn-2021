<?php

namespace Magebase\Example\Observer;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class MageBaseExample implements ObserverInterface
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * MageBaseExample constructor.
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        ProductRepositoryInterface $productRepository
    )
    {
        $this->productRepository = $productRepository;
    }

    public function execute(Observer $observer)
    {
        $product = $this->productRepository->getById(1);

        echo "\n";

        echo "magebase observer";

        echo "\n";

        echo $product->getName();

        echo "\n";

    }
}
