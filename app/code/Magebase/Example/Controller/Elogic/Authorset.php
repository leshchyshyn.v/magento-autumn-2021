<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magebase\Example\Controller\Elogic;

use Magebase\Example\Api\AuthorRepositoryInterface;
use Magebase\Example\Api\Data\AuthorInterface;
use Magebase\Example\Model\Author;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ActionInterface;

class Authorset extends Action
{
    /**
     * @var Author
     */
    private $author;
    /**
     * @var AuthorRepositoryInterface
     */
    private $authorRepository;

    /**
     * Authorset constructor.
     * @param Context $context
     * @param AuthorInterface $author
     * @param AuthorRepositoryInterface $authorRepository
     */
    public function __construct(
        Context $context,
        AuthorInterface $author,
        AuthorRepositoryInterface $authorRepository
    )
    {
        parent::__construct($context);
        $this->author = $author;
        $this->authorRepository = $authorRepository;
    }

    public function execute()
    {


        $this->author = $this->authorRepository->getById(1);
        $this->author->setData("full_name","Seneca");
        $this->author->setData("living_years","1700-1800");
        $this->authorRepository->save($this->author);
//        $this->author->save();
        echo "creating1 author";

    }
}
