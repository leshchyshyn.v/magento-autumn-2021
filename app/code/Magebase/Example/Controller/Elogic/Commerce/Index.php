<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magebase\Example\Controller\Elogic\Commerce;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ActionInterface;

class Index extends Action
{
    public function execute()
    {
        echo "ELOGIC Commerce INDEX ACTION 2021";
    }
}
