<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magebase\Example\Controller\Elogic\Commerce;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ActionInterface;

class Elogic extends Action
{
    public function execute()
    {
        echo "ELOGIC Commerce ELOGIC ACTION 2021";
    }
}
