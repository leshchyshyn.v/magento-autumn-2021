<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magebase\Example\Controller\Elogic;

use Magebase\Example\Api\Data\AuthorInterface;
use Magebase\Example\Model\Author;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ActionInterface;

class Authorget extends Action
{
    /**
     * @var Author
     */
    private $author;

    public function __construct(
        Context $context,
        AuthorInterface $author
    )
    {
        parent::__construct($context);
        $this->author = $author;
    }

    public function execute()
    {
        $this->author->load(1);
        var_dump($this->author->getData());
    }
}
